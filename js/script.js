var API_PATH = 'http://localhost/hero/web/';

$(document).ready(function(){

    var movements = 0;
    var marker = $('#map-marker');
    var movespeed_disabled = 0;
    $('#welcomeModal').modal('show');

    //detect key press in order to move the hero
    $(document).keypress(function(e) {

        if(!movespeed_disabled) {
            $('#welcomeModal').modal('hide');
            switch (e.which) {
                case 119:
                    moveUp();
                    break;
                case 97:
                    moveLeft();
                    break;
                case 115:
                    moveDown();
                    break;
                case 100:
                    moveRight();
                    break;
            }
            fightStart();
        }
    });

    function moveUp(){

        var top = marker.css('top').replace('px','');
        if(top >= 0){
            movements++;
            marker.css('bottom', '+=10px');
        }
    }

    function moveLeft(){

        var left = marker.css('left').replace('px','');
        if(left >= 0){
            movements++;
            marker.css('left', '-=10px');
        }
    }

    function moveDown(){

        var bottom = marker.css('bottom').replace('px','');
        if(bottom >= 85){
            movements++;
            marker.css('bottom', '-=10px');
        }

    }

    function moveRight(){

        var right = marker.css('right').replace('px','');
        if(right >= 10){
            movements++;
            marker.css('left', '+=10px');
        }
    }

    function fightStart(){

        if(movements >= 4){

            movements = 0;

            //stop hero from moving
            movespeed_disabled = 1;

            $('#fightSummaryModal').modal('show');

            //get response from api
            setTimeout(function(){
                fightSummary();
            }, 3000);

        }

    }

    function fightSummary(){
        $.ajax({

            url: API_PATH,
            dataType: 'json',
            success:function(data){

                var player1 = data.players[1].name;
                var player2 = data.players[2].name;
                var winner = data.winner;

                $("#fightSummaryModal .modal-dialog").addClass('modal-lg');
                $("#fightSummaryModal .modal-title").html('Fight summary:');
                $("#fightSummaryModal .modal-body").html('<p>' + player1 + ' vs ' + player2 + '</p>');
                $("#fightSummaryModal .modal-body").append('<p>Winner: ' + winner + '</p>');

                addSummaryTable(data);
            },
            error:function(){
                alert('Unable to connect on api!');
            }

        });
    }

    function addSummaryTable(data){

        var tbody = ' ';
        data.rounds.forEach(function(round){

            tbody += '<tr>'+
                '<td>' + round.name + '</td>' +
                '<td>' + round.players[1].name + ' vs ' + round.players[2].name + '</td>' +
                '<td class="text-center">'+ round.fight.initial_damage +'</td>' +
                '<td class="text-center">'+ round.fight.enchanted_damage +'</td>' +
                '<td class="text-center">'+ round.fight.blocked_damage +'</td>' +
                '<td class="text-center">'+ round.fight.sent_damage +'</td>' +
                '<td>'+ round.skills.join(', ') +'</td>' +
                '<td class="text-center">'+ round.players[2].stats.health +'</td>' +
                '</tr>';
        });

        var table = '<table class="table table-striped">' +
            '<thead><tr>'+
            '<th>Round</th>' +
            '<th>Duel</th>' +
            '<th class="text-center">Initial dmg</th>' +
            '<th class="text-center">Enchanted dmg</th>' +
            '<th class="text-center">Blocked dmg</th>' +
            '<th class="text-center">Sent dmg</th>' +
            '<th>Skills</th>' +
            '<th class="text-center">Health</th>' +
            '</tr></thead>' +
            '<tbody>' +
            tbody +
            '</tbody>' +
            '</table>';

        $("#fightSummaryModal .modal-body").append('<div class="rounds">' + table + '</div>');

    }

    //on fight modal close hero can move again
    $('#fightSummaryModal').on('hidden.bs.modal', function () {
        movespeed_disabled = 0;
        $("#fightSummaryModal .modal-dialog").removeClass('modal-lg');
        $("#fightSummaryModal .modal-title").html('You have been engaged in a fight');
        $("#fightSummaryModal .modal-body").html('<img class="ajax-loader" src="images/fight.gif">');

    })



});


